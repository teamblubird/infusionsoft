<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://blubirdinteractive.com/
 * @since      1.0.0
 *
 * @package    Infusion_Soft
 * @subpackage Infusion_Soft/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Infusion_Soft
 * @subpackage Infusion_Soft/includes
 * @author     Blubird Interactive Ltd <mail2technerd@gmail.com>
 */
class Infusion_Soft_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
