<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://blubirdinteractive.com/
 * @since      1.0.0
 *
 * @package    Infusion_Soft
 * @subpackage Infusion_Soft/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Infusion_Soft
 * @subpackage Infusion_Soft/includes
 * @author     Blubird Interactive Ltd <mail2technerd@gmail.com>
 */
class Infusion_Soft_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'infusion-soft',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
