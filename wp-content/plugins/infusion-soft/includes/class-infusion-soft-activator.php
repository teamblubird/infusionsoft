<?php

/**
 * Fired during plugin activation
 *
 * @link       https://blubirdinteractive.com/
 * @since      1.0.0
 *
 * @package    Infusion_Soft
 * @subpackage Infusion_Soft/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Infusion_Soft
 * @subpackage Infusion_Soft/includes
 * @author     Blubird Interactive Ltd <mail2technerd@gmail.com>
 */
class Infusion_Soft_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
