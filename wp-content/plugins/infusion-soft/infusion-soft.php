<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://blubirdinteractive.com/
 * @since             4.0.0
 * @package           Infusion_Soft
 *
 * @wordpress-plugin
 * Plugin Name:       Infusion Soft
 * Plugin URI:        https://blubirdinteractive.com/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Blubird Interactive Ltd
 * Author URI:        https://blubirdinteractive.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       infusion-soft
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'INFUSION_SOFT_VERSION', '4.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-infusion-soft-activator.php
 */
function activate_infusion_soft() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-infusion-soft-activator.php';
	Infusion_Soft_Activator::activate();
	tableCreateAndsettingsData();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-infusion-soft-deactivator.php
 */
function deactivate_infusion_soft() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-infusion-soft-deactivator.php';
	Infusion_Soft_Deactivator::deactivate();
	tableDeleteAndsettingsData();
}

register_activation_hook( __FILE__, 'activate_infusion_soft' );
register_deactivation_hook( __FILE__, 'deactivate_infusion_soft' );
$classes = glob(plugin_dir_path( __FILE__ ).'class/autoload/*.php');
if ($classes) {
	foreach ($classes as $class) {
		require_once $class;
	}
}

function tableCreateAndsettingsData(){
	ISDb::createAccessTokenTable();
	ISDb::createSettingsCredTable();
	ISDb::SettingsCredInsert();
	ISDb::createContactTable();
	ISDb::createTagTable();
	ISDb::createTagsOfContactTable();
	ISDb::createTaggedContactsTable();
}

function tableDeleteAndsettingsData(){
	ISDb::deleteAccessTokenTable();
	ISDb::deleteSettingsCredTable();
	ISDb::deleteContactTable();
	ISDb::deleteTagTable();
	ISDb::deleteTagsOfContactTable();
	ISDb::deleteTaggedContactsTable();
}

//First Time Token Data Insert 
function firstTimeInsert($data){
	return ISDb::firstTimeInsert($data);
}

//First Time Settings Data Insert 
function SettingsCredInsert(){
	return ISDb::SettingsCredInsert();
}

//Get Data From a Table
function getTableData($tableName){
	return ISDb::getTableData($tableName);
}

//Get Data From Token Table
function getAccessTokenFromDb(){
	return ISDb::getAccessTokenFromDb();
}

//Get Data From Settings Table
function getSettingsCred(){
	return ISDb::getSettingsCred();
}

//Refresh Access Token When Acees Token Expires
function getRefreshToken($infusionClientId,$infusionClientSecret,$refresh_token){
	return ISHelper::getRefreshToken($infusionClientId,$infusionClientSecret,$refresh_token);
}

//Update New Access Token
function updateTokenTable($newToken){
	return ISDb::updateTokenTable($newToken);
}

//Get All The Data of a Connected Profile with Access Token
function getProfile($accessToken){
	return ISHelper::getProfile($accessToken);
}

// //Get All The Data of a Connected Company with Access Token
function getCompany($accessToken){
	return ISHelper::getCompany($accessToken);
}

//Get All Countries
function getCountries($accessToken){
	return ISHelper::getCountries($accessToken);
}

//Get All The Contacts of a Connected Profile with Access Token
function getContacts($accessToken){
	return ISHelper::getContacts($accessToken);
}

//Get All The Contacts From Db
function getContactDataFromDb(){
	return ISDb::getContactDataFromDb();
}

//Get All The Tags From Db
function getTagDataFromDb(){
	return ISDb::getTagDataFromDb();
}

//Get All The Tags of a Connected Profile with Access Token
function getTags($accessToken){
	return ISHelper::getTags($accessToken);
}

//Get All The Tagged Contacts with Access Token
function taggedContacts($accessToken,$tagId){
	return ISHelper::taggedContacts($accessToken,$tagId);
}

//Get All The Tagged Contacts From Db By TagId
function taggedContactsByTagId($tagId){
	return ISDb::taggedContactsByTagId($tagId);
}

//Get All The Tags of a contact From Db By contactId
function tagsOfContactByContactId($contactId){
	return ISDb::tagsOfContactByContactId($contactId);
}

//Get All The Tags of a contact From Db By contactId
function getContactIdDb(){
	return ISDb::getContactIdDb();
}

//Get All The Tags of a Contact From Db By ContactId
function getTagsByContactId($contactId,$access_token){
	return ISHelper::getTagsByContactId($contactId,$access_token);
}

//Get All The Tag's Contacts Count of a Specific Tag with Access Token
function getTaggedContactsCount($accessToken,$tagId){
	return ISHelper::getTaggedContactsCount($accessToken,$tagId);
}

//Token Validation
function isValidToken($created_at,$expires_in){
	return ISHelper::isvalidToken($created_at,$expires_in);
}

//Get Client ID
function getClientId(){
	return ISHelper::getClientId();
}

//Sync Contacts
function sync_contacts($access_token){
	return ISHelper::sync_contacts($access_token);
}

//Sync Tags
function sync_tags($access_token){
	return ISHelper::sync_tags($access_token);
}

//Sync Tagged Contacts
function syncTaggedContacs($access_token){
	return ISHelper::syncTaggedContacs($access_token);
}

//Sync Tags of a Contact
function syncTagsOfContact($access_token){
	return ISHelper::syncTagsOfContact($access_token);
}

//Get Client Secret
function getClientSecret(){
	return ISHelper::getClientSecret();
}


$templates = glob(plugin_dir_path( __FILE__ ).'admin/template-parts/*.php');
if ($templates) {
	foreach ($templates as $template) {
		require_once $template;
	}
}
//Show Connected Profile Details
function showConnectedProfile($profile,$contacts,$access_token){
	return ISConnectedprofile::showConnectedProfile($profile,$contacts,$access_token);
}
/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-infusion-soft.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_infusion_soft() {

	$plugin = new Infusion_Soft();
	$plugin->run();

}
run_infusion_soft();

