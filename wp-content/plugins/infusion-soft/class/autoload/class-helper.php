<?php 
class ISHelper {
	public static function isvalidToken($created_at,$expires_in){
		$presentTime = current_time('timestamp');
		$timeDiff = abs($presentTime - $created_at);
		if (abs($timeDiff) > $expires_in) {
			return '401';
		} else {
			return '200';
		}
	}

	public static function getRefreshToken($infusionClientId,$infusionClientSecret,$refresh_token){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/token",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=".$refresh_token,
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		    "Authorization: Basic ". base64_encode("".$infusionClientId.":".$infusionClientSecret),
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$results = $response;
			$results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
			// echo "<pre>"; print_r($results); echo "</pre>";exit();
			return $results;
		}
	}

	public static function getProfile($accessToken){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			  	CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/oauth/connect/userinfo?access_token=".$accessToken,
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "GET",
			  	CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "postman-token: fa58bee9-207a-4294-fd25-778e4e84c3ef"
		 	 	),
			));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	  		echo "cURL Error #:" . $err;
		} else {
	  		$results = $response;
	  		$results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	  		// echo "<pre>"; print_r($results); echo "</pre>";
	  		return $results;
		}
	}

	public static function getCompany($accessToken){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			  	CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/account/profile?access_token=".$accessToken,
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "GET",
			  	CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "postman-token: fa58bee9-207a-4294-fd25-778e4e84c3ef"
		 	 	),
			));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	  		echo "cURL Error #:" . $err;
		} else {
	  		$results = $response;
	  		$results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	  		// echo "<pre>"; print_r($results); echo "</pre>";
	  		return $results;
		}
	}

	public static function getContacts($accessToken){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			  	CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/contacts?limit=55&access_token=".$accessToken,
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "GET",
			  	CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "postman-token: fa58bee9-207a-4294-fd25-778e4e84c3ef"
		 	 	),
			));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	  		echo "cURL Error #:" . $err;
		} else {
	  		$results = $response;
	  		$results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	  		$results = $results['contacts'];
	  		// echo "<pre>"; print_r($results); echo "</pre>";
	  		$results = self::multiArraytoSingle($results);
	  		return $results;
		}
	}

	public static function getCountries($accessToken){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			  	CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/locales/countries?access_token=".$accessToken,
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "GET",
			  	CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "postman-token: fa58bee9-207a-4294-fd25-778e4e84c3ef"
		 	 	),
			));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	  		echo "cURL Error #:" . $err;
		} else {
	  		$results = $response;
	  		$results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	  		$results = $results['countries'];
	  		// echo "<pre>"; print_r($results); echo "</pre>";
	  		return $results;
		}
	}

	public static function addContacts($dbToken){
		$data = array (
		  "addresses" => array (
		      "country_code" => "+880",
		  ),
		  "email_addresses" => array (
		      "email" => "testrest2@live.com",
		      "field" => "EMAIL1"
		  )
		);
		$parameters = json_encode($data, true);
		//var_dump ($parameters);
		$ch = curl_init();


		curl_setopt($ch, CURLOPT_URL, "https://api.infusionsoft.com/crm/rest/v1/contacts?access_token=$dbToken");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "content-type:application/json;charset=UTF-8",
		  "Accept:application/json, */*",
		  
		));
		curl_setopt($ch, CURLOPT_POST, true); 
		   curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
		$response = curl_exec($ch);
		$info = curl_getinfo($ch);

		curl_close($ch);
		//HTTP header response code
		$responsecode = $info['http_code'];
		$json = json_decode($response, JSON_PRETTY_PRINT);
		print_r($json);
	}

	//4 Layer Multi Dimensional Array to Single Array
	public static function multiArraytoSingle($array){
		$singleArray = array();
		$single = array();
		if (is_array($array)) {
			foreach ($array as $key => $value) {
				foreach ($value as $newkey => $newvalue) {
					if (is_array($newvalue)) {
						foreach ($newvalue as $anotherkey => $anothervalue){
							if (is_array($anothervalue)) {
								foreach ($anothervalue as $finalkey => $finalvalue) {
									$single[$finalkey]= $finalvalue;
									// echo $key." : ".$value."<br>";
								}
							}
						}
					} else {
						$single[$newkey]= $newvalue;
						// echo $key." : ".$value."<br>";
					} 
				}
				// echo "<hr>";
				// echo "<pre>";print_r($singleContact);echo "</pre>"; 
				array_push($singleArray, $single);
			}
		}
		return $singleArray;
	}

	public static function getTags($accessToken){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			  	CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/tags?access_token=".$accessToken,
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "GET",
			  	CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "postman-token: fa58bee9-207a-4294-fd25-778e4e84c3ef"
		 	 	),
			));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	  		echo "cURL Error #:" . $err;
		} else {
	  		$results = $response;
	  		$results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	  		// echo "<pre>"; print_r($results); echo "</pre>";
	  		$results = $results['tags'];
	  		return $results;
		}
	}

	public static function getTaggedContactsCount($accessToken,$tagId){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/tags/".$tagId."/contacts?access_token=".$accessToken,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "postman-token: 8cc62974-f0b2-5f5f-44ee-1251ce34b1d4"
		  ),
		));

		$response = curl_exec($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  // echo $response;
		  $results = $response;
	      $results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	      return $count = $results['count'];
	      // echo "<pre>"; print_r($results); echo "</pre>";
		}
	}

	public static function taggedContacts($accessToken,$tagId){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/tags/".$tagId."/contacts?access_token=".$accessToken,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "postman-token: 8cc62974-f0b2-5f5f-44ee-1251ce34b1d4"
		  ),
		));

		$response = curl_exec($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
			// echo $response;
			$results = $response;
			$results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
			$count = $results['count'];
			if ($count > 0) {
				return $contacts = $results['contacts'];
			}
	
		}
	}

	public static function getTagsByContactId($contactId,$access_token){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/contacts/".$contactId."/tags?access_token=".$access_token,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "postman-token: 8371dbff-346d-3aec-9c66-0a138c8d9225"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  $results = $response;
		  $results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
		  return $results;
		}
	}

	public static function getClientId(){
		$settingsCred = getSettingsCred();
		return $settingsCred[0]->client_id;
	}

	public static function getClientSecret(){
		$settingsCred = getSettingsCred();
		return $settingsCred[0]->client_secret;
	}

	public static function sync_contacts($access_token){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		global $wpdb;
		$table = $wpdb->prefix;
		$getContactDataFromDb = getContactDataFromDb();
		$contacts = getContacts($access_token);
		$results=array_diff_assoc($contacts,$getContactDataFromDb);
		// echo "<pre>";print_r($results);echo "</pre>";exit();
		foreach ($results as $result) {
			$query = $wpdb->insert($wpdb->prefix.'contacts_infusionSoft', array(
			    'id' 			=> $result['id'],
			    'email' 		=> $result['email'],
			    'line1' 		=> $result['line1'], 
			    'line2' 		=> $result['refresh_token'], 
			    'locality' 		=> $result['locality'], 
			    'region' 		=> $result['region'], 
			    'postal_code' 	=> $result['postal_code'], 
			    'zip_code' 		=> $result['zip_code'], 
			    'zip_four' 		=> $result['zip_four'], 
			    'country_code' 	=> $result['country_code'], 
			    'owner_id' 		=> $result['owner_id'], 
			    'middle_name' 	=> $result['middle_name'], 
			    'given_name' 	=> $result['given_name'], 
			    'email_status' 	=> $result['email_status'], 
			    'company' 		=> $result['company'], 
			    'family_name' 	=> $result['family_name'], 
			    'last_updated' 	=> $result['last_updated'], 
			    'date_created' 	=> $result['date_created'] 
			)); 
		}
		return $query;
	}

	public static function sync_tags($access_token){
		global $wpdb;
		$table = $wpdb->prefix;
		$tags = getTags($access_token);
		// echo "<pre>";print_r($results);echo "</pre>";exit();
		foreach ($tags as $tag) {
			$tagCount = getTaggedContactsCount($_POST['access_token'], $tag['id']);
			$query = $wpdb->insert($wpdb->prefix.'tags_infusionSoft', array(
			    'id' 					=> $tag['id'],
			    'tag_name' 				=> $tag['name'],
			    'description' 			=> $tag['description'], 
			    'cat_id' 				=> $tag['category']['id'], 
			    'cat_name' 				=> $tag['category']['name'], 
			    'cat_description' 		=> $tag['category']['description'],
			    'count' 		=> $tagCount
			)); 
		}
		return $query;
	}

	public static function syncTaggedContacs($access_token){
		global $wpdb;
		$table = $wpdb->prefix;
		$getTagDataFromDb = getTagDataFromDb();
		// echo "<pre>";print_r($getTagDataFromDb);echo "</pre>";exit();
		foreach ($getTagDataFromDb as $getTag) {
			$results = taggedContacts($access_token,$getTag['id']);
			// echo "<pre>";print_r($results);echo "</pre>";
			foreach ($results as $result) {
				$query = $wpdb->insert($wpdb->prefix.'tagged_contacts_infusionSoft', array(
				    'tag_id' 				=> $getTag['id'],
				    'contact_id' 			=> $result['contact']['id'],
				    'first_name' 			=> $result['contact']['first_name'], 
				    'last_name' 			=> $result['contact']['last_name'], 
				    'email' 				=> $result['contact']['email']
				));
			}
		}
		return $query;
	}

	public static function syncTagsOfContact($access_token){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		global $wpdb;
		$table = $wpdb->prefix;
		$getContactDataFromDb = getContactDataFromDb();
		foreach ($getContactDataFromDb as $contact) {
			$contactId = $contact['id'];
			$results = getTagsByContactId($contactId, $access_token);
			// echo "<pre>";print_r($results);echo "</pre>";exit();
			$tagsData = $results['tags'];
			foreach ($tagsData as $tagData) {
				$query = $wpdb->insert($wpdb->prefix.'tags_of_contact_infusionSoft', array(
				    'tag_id' 			=> $tagData['tag']['id'],
				    'contact_id' 		=> $contactId,
				    'tag_name' 			=> $tagData['tag']['name'], 
				    'category' 			=> $tagData['tag']['category']
				));
			}
		}
		return $query;
	}
}
new ISHelper();