<?php
class ISAjax {
	function __construct() {

		add_action( 'wp_ajax_authenticateWithCred', [$this, 'authenticateWithCred'] );
		add_action( 'wp_ajax_nopriv_authenticateWithCred', [$this, 'authenticateWithCred'] );

		add_action( 'wp_ajax_requestAccessToken', [$this, 'requestAccessToken'] );
		add_action( 'wp_ajax_nopriv_requestAccessToken', [$this, 'requestAccessToken'] );

		add_action( 'wp_ajax_addContact', [$this, 'addContact'] );
		add_action( 'wp_ajax_nopriv_addContact', [$this, 'addContact'] );

		add_action( 'wp_ajax_getProvinceLists', [$this, 'getProvinceLists'] );
		add_action( 'wp_ajax_nopriv_getProvinceLists', [$this, 'getProvinceLists'] );

		add_action( 'wp_ajax_updateContactById', [$this, 'updateContactById'] );
		add_action( 'wp_ajax_nopriv_updateContactById', [$this, 'updateContactById'] );

		add_action( 'wp_ajax_deleteContactById', [$this, 'deleteContactById'] );
		add_action( 'wp_ajax_nopriv_deleteContactById', [$this, 'deleteContactById'] );

		add_action( 'wp_ajax_addTag', [$this, 'addTag'] );
		add_action( 'wp_ajax_nopriv_addTag', [$this, 'addTag'] );

		add_action( 'wp_ajax_addCat', [$this, 'addCat'] );
		add_action( 'wp_ajax_nopriv_addCat', [$this, 'addCat'] );

		add_action( 'wp_ajax_saveConnectAppCred', [$this, 'saveConnectAppCred'] );
		add_action( 'wp_ajax_nopriv_saveConnectAppCred', [$this, 'saveConnectAppCred'] );

		add_action( 'wp_ajax_disconnectApp', [$this, 'disconnectApp'] );
		add_action( 'wp_ajax_nopriv_disconnectApp', [$this, 'disconnectApp'] );

		add_action( 'wp_ajax_checkUserProvidedCred', [$this, 'checkUserProvidedCred'] );
		add_action( 'wp_ajax_nopriv_checkUserProvidedCred', [$this, 'checkUserProvidedCred'] );

		add_action( 'wp_ajax_showTagsByContactId', [$this, 'showTagsByContactId'] );
		add_action( 'wp_ajax_nopriv_showTagsByContactId', [$this, 'showTagsByContactId'] );

		add_action( 'wp_ajax_removeTagFromContact', [$this, 'removeTagFromContact'] );
		add_action( 'wp_ajax_nopriv_removeTagFromContact', [$this, 'removeTagFromContact'] );

		add_action( 'wp_ajax_taggedContactsById', [$this, 'taggedContactsById'] );
		add_action( 'wp_ajax_nopriv_taggedContactsById', [$this, 'taggedContactsById'] );

		add_action( 'wp_ajax_syncAll', [$this, 'syncAll'] );
		add_action( 'wp_ajax_nopriv_syncAll', [$this, 'syncAll'] );

		add_action( 'wp_ajax_syncAllContacts', [$this, 'syncAllContacts'] );
		add_action( 'wp_ajax_nopriv_syncAllContacts', [$this, 'syncAllContacts'] );
		
	}

	function authenticateWithCred() {
		// echo "<br>";print_r($_POST);echo "</pre>";exit();
		$client_id = $_POST['client_id'];
		$client_secret = $_POST['client_secret'];
		$redirect_uri = $_POST['redirect_uri'];

		if ($client_id != "" && $client_secret != "" && $redirect_uri != "") {
			$api_status = 200;
			$authUrl = "https://signin.infusionsoft.com/app/oauth/authorize?client_id=".$_POST['client_id']."&client_secret=".$_POST['client_secret']."&redirect_uri=".$_POST['redirect_uri']."&response_type=code&scope=full";
			
		} else {
			$api_status = 401;
		}
		echo json_encode(['status'=>$api_status,'client_id'=>@$client_id,'client_secret'=>@$client_secret,'authUrl'=>@$authUrl]);
		wp_die();
	}

	function requestAccessToken() {
		$client_id = $_POST['client_id'];
		$client_secret = $_POST['client_secret'];
		$redirect_uri = $_POST['redirect_uri'];
		$code = $_POST['code'];
		$redirect_uri = $_POST['redirect_uri'];
		// echo "<br>";print_r($_POST);echo "</pre>";exit();
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/token",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "client_id=".$client_id."&client_secret=".$client_secret."&code=".$code."&grant_type=authorization_code&redirect_uri=".$redirect_uri,
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/x-www-form-urlencoded",
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			$results = $response;
			$results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
			// echo "<pre>"; print_r($results); echo "</pre>";exit();
			// return $results;
		}
		$insertTokens = firstTimeInsert($results);

		if ($insertTokens == 1) {
			$api_status = 200;
		} else {
			$api_status = 401;
		}

		echo json_encode(['status'=>$api_status,'access_token'=>$results['access_token']]);
		wp_die();
	}

	function addContact() {
		// echo "<br>";print_r($_POST);echo "</pre>";exit();
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/contacts?access_token=".$_POST['access_token'],
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"addresses\": [{\"country_code\": \"".$_POST['addContactCountryCode']."\", \"field\": \"BILLING\", \"line1\": \"".$_POST['addContactAddress']."\", \"line2\": \"\", \"locality\": \"".$_POST['addContactLocality']."\", \"postal_code\": \"".$_POST['addContactPostal_code']."\", \"region\": \"".$_POST['addContactRegion']."\" } ], \"given_name\": \"".$_POST['addContactName']."\", \"email_addresses\": [{\"email\": \"".$_POST['addContactEmail']."\", \"field\": \"EMAIL1\" }] }",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/json",
		    "postman-token: 93fdf583-a7bf-c538-5400-dd6432d24667"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  $results = $response;
		  $results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
		  $status = $http_status;
		  if ($status == 201) {
		  	global $wpdb;
				$query_insert = $wpdb->insert($wpdb->prefix.'contacts_infusionSoft', array(
						'id' 		=> $results['id'],
						'email' 		=> $_POST['addContactEmail'],
					    'line1' 		=> $_POST['addContactAddress'], 
					    'locality' 		=> $_POST['addContactLocality'], 
					    'region' 		=> $_POST['addContactRegion'], 
					    'postal_code' 	=> $_POST['addContactPostal_code'], 
					    'zip_code' 		=> $_POST['addContactPostal_code'], 
					    'country_code' 	=> $_POST['addContactCountryCode'], 
					    'given_name' 	=> $_POST['addContactName']
					));
				echo $query_insert;
		  }
		}
		
		wp_die();
	}

	function updateContactById(){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		$curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/contacts/".$_POST['contactId']."?access_token=".$_POST['access_token'],
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "PATCH",
	      CURLOPT_POSTFIELDS => "{\"addresses\": [{\"country_code\": \"".$_POST['updateContactCountryCode']."\", \"field\": \"BILLING\", \"line1\": \"".$_POST['updateContactAddress']."\", \"line2\": \"\", \"locality\": \"".$_POST['updateContactLocality']."\", \"postal_code\": \"".$_POST['updateContactPostal_code']."\", \"region\": \"".$_POST['updateContactRegion']."\" } ], \"given_name\": \"".$_POST['updateContactName']."\",\"phone_numbers\": [{\"extension\": \"\", \"field\": \"PHONE1\", \"number\": \"".$_POST['updateContactPhone']."\"} ], \"email_addresses\": [{\"email\": \"".$_POST['updateContactEmail']."\", \"field\": \"EMAIL1\" }] }",
	      CURLOPT_HTTPHEADER => array(
	        "cache-control: no-cache",
	        "content-type: application/json"
	      ),
	    ));

	    $response = curl_exec($curl);
	    $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	    $err = curl_error($curl);

	    curl_close($curl);

	    if ($err) {
	      echo "cURL Error #:" . $err;
	    } else {
	      $results = $http_status;
	      $results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	      // echo json_encode(['status'=>$results]);
			if ($results == 200) {
	      		global $wpdb;
				$query_update = $wpdb->update($wpdb->prefix.'contacts_infusionSoft', array(
						'email' 		=> $_POST['updateContactEmail'],
					    'line1' 		=> $_POST['updateContactAddress'], 
					    'locality' 		=> $_POST['updateContactLocality'], 
					    'region' 		=> $_POST['updateContactRegion'], 
					    'postal_code' 	=> $_POST['updateContactPostal_code'], 
					    'zip_code' 		=> $_POST['updateContactPostal_code'], 
					    'country_code' 	=> $_POST['updateContactCountryCode'], 
					    'given_name' 	=> $_POST['updateContactName']
					),array('id' => $_POST['contactId']));
				if ($query_update == 1) {
					echo json_encode(['status'=>$results]);
				}
			}
	    }
	    wp_die();
	}

	function getProvinceLists(){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/locales/countries/".$_POST['country_code']."/provinces?access_token=".$_POST['access_token'],
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "postman-token: 8cc62974-f0b2-5f5f-44ee-1251ce34b1d4"
		  ),
		));

		$response = curl_exec($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  // echo $response;
		  $results = $response;
	      $results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	      $provinces = $results['provinces'];
	      echo json_encode(['status'=>$http_status,'provinces'=>$provinces]);
	      // echo "<pre>"; print_r($provinces); echo "</pre>";
		}
		wp_die();
	}

	function addTag(){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/tags?access_token=".$_POST['access_token'],
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"description\": \"".$_POST['tagDescription']."\", \"name\": \"".$_POST['tagName']."\"}",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/json",
		    "postman-token: c6112c1d-2d27-9e11-dc14-bbf87785a126"
		  ),
		));

		$response = curl_exec($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  $results = $http_status;
		  // print_r($results);
		  echo json_encode(['status'=>$results]);

		}
		wp_die();
	}

	function deleteContactById() {
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		$curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/contacts/".$_POST['contactId']."?access_token=".$_POST['access_token'],
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "DELETE",
	      CURLOPT_HTTPHEADER => array(
	        "cache-control: no-cache",
	        "content-type: application/json"
	      ),
	    ));

	    $response = curl_exec($curl);
	    $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	    $err = curl_error($curl);

	    curl_close($curl);

	    if ($err) {
	      echo "cURL Error #:" . $err;
	    } else {
	      $results = $http_status;
	      $results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	      echo json_encode(['status'=>$results]);
	      // echo "<pre>"; print_r($results); echo "</pre>";
	      // return $results;
	    }
	    wp_die();
	}

	function addCat(){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/tags/categories?access_token=".$_POST['access_token'],
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "{\"description\": \"".$_POST['catDescription']."\", \"name\": \"".$_POST['catName']."\"}",
		  CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  $results = $http_status;
		  // $results = $response;
		  // print_r($results);
		  echo json_encode(['status'=>$results]);

		}
		wp_die();
	}

	function saveConnectAppCred(){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		global $wpdb;
		$data = getTableData('infusion_tokenDetails');
		if (empty($data)) {
			if ($_POST['access_token'] != "" && $_POST['token_type'] != "" && $_POST['expires_in'] != "" && $_POST['refresh_token'] != "" && $_POST['scope'] != "")  {
				$wpdb->insert($wpdb->prefix.'infusion_tokenDetails', array(
				    'access_token' => $_POST['access_token'],
				    'token_type' => $_POST['token_type'],
				    'expires_in' => $_POST['expires_in'], 
				    'refresh_token' => $_POST['refresh_token'], 
				    'scope' => $_POST['scope'], 
				));
				echo json_encode(['status'=>200]);
			} else {
				echo json_encode(['status'=>401]);
			}
		} else {
			updateTokenTable($_POST);
			echo json_encode(['status'=>200]);
		}
		wp_die();
	}

	function checkUserProvidedCred(){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			  	CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/oauth/connect/userinfo?access_token=".$_POST['access_token'],
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "GET",
			  	CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "postman-token: fa58bee9-207a-4294-fd25-778e4e84c3ef"
		 	 	),
			));
		$response = curl_exec($curl);
		$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	  		echo "cURL Error #:" . $err;
		} else {
	  		$results = $http_status;
	  		// echo "<pre>"; print_r($results); echo "</pre>";
	  		echo $results;
		}
		wp_die();
	}

	function removeTagFromContact(){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		$curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => "https://api.infusionsoft.com/crm/rest/v1/contacts/".$_POST['contactId']."/tags/".$_POST['tagId']."?access_token=".$_POST['access_token'],
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_ENCODING => "",
	      CURLOPT_MAXREDIRS => 10,
	      CURLOPT_TIMEOUT => 30,
	      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	      CURLOPT_CUSTOMREQUEST => "DELETE",
	      CURLOPT_HTTPHEADER => array(
	        "cache-control: no-cache",
	        "content-type: application/json"
	      ),
	    ));

	    $response = curl_exec($curl);
	    $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	    $err = curl_error($curl);

	    curl_close($curl);

	    if ($err) {
	      echo "cURL Error #:" . $err;
	    } else {
	      $results = $http_status;
	      $results = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $results), true );
	      // echo "<pre>";print_r($results);echo "</pre>";exit();
	      echo json_encode(['status'=>$results]);
	    }
		wp_die();
	}

	function taggedContactsById(){
		$results = taggedContactsByTagId($_POST['tagId']);
		// echo "<pre>";print_r($results);echo "</pre>";exit();
		$html = "";
		$SI = 1;
		foreach ($results as $result) {
			$html = '<tr>
				<td>'.$SI.'</td>
				<td>'.$result['first_name']." ".$result['last_name'].'</td>
				<td>'.$result['email'].'</td>
			</tr>';
			$SI++;
			echo $html;
		}
		wp_die();
	}

	function showTagsByContactId(){
		$results = tagsOfContactByContactId($_POST['contactId']);
		$count = count($results);
		// echo "<pre>";print_r($results);echo "</pre>";exit();
		if ($count > 0) {
			$SI = 1;
			foreach ($results as  $result) {
			?>
			<tr class="taggedContact" id="taggedContact<?php echo $SI;?>">
				<td><?php echo $SI; ?></td>
				<td class="hidden"><input type="text" class="tagId" value="<?php echo $result['id']; ?>"></td>
				<td class="hidden"><input type="text" class="contactId" value="<?php echo $_POST['contactId']; ?>"></td>
				<td><?php echo $result['tag_name']; ?></td>
				<td><?php echo $result['category']; ?></td>
				<td><input type="submit" class="btn btn-danger removeTag" value="Remove Tag"></td>
			</tr>
			<?php
			  	$SI++;			
			} ?>
			<script>
	  			(function($){
	  				'use strict';
	  				$(".removeTag").on("click",function(){
	  					var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";
						var rowId = $(this).parents("tr").attr("id");
						var tagId = $("#" + rowId + " .tagId").val();
						var contactId = $("#" + rowId + " .contactId").val();

						var ajaxData = {
					          'action'  	: 'removeTagFromContact',
					          'access_token': '<?php echo $access_token; ?>',
					          'tagId'		: tagId,
					          'contactId'	: contactId
					    }

					    $.ajax({
							url: ajaxUrl,
							method: 'POST',
							data: ajaxData,
							success: function ( data ) {
								console.log(data);
								$("#" + rowId).fadeOut( "slow" );
											
							},
			                error: function(e) {
			               		console.log(e);
			                }
			            });
					});
	  			})(jQuery);
	  		</script><?php
		}
		wp_die();
	}

	function syncAll(){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		$contactsSync = sync_contacts($_POST['access_token']);
		if ($contactsSync == 1) {
			$tagsSync = sync_tags($_POST['access_token']);
			if ($tagsSync == 1) {
				$syncTaggedContacs = syncTaggedContacs($_POST['access_token']);
				if ($syncTaggedContacs == 1) {
					$syncTagsOfContact = syncTagsOfContact($_POST['access_token']);
					if ($syncTagsOfContact == 1) {
						echo json_encode(['status'=>200]);
					} else {
						echo json_encode(['status'=>401]);
					}
				}
			}

		}
		wp_die();
	}

	function syncAllContacts(){
		// echo "<pre>";print_r($_POST);echo "</pre>";exit();
		global $wpdb;
		$table = $wpdb->prefix;
		$getContactIdDb = getContactIdDb();
		$getContactIdDb = array_column($getContactIdDb,'id');

		$contacts = getContacts($_POST['access_token']);

		foreach ($contacts as $contact) {
			if(in_array($contact['id'], $getContactIdDb))
			{
				$query_update = $wpdb->update($wpdb->prefix.'contacts_infusionSoft', array(
						'email' 		=> $contact['email'],
					    'line1' 		=> $contact['line1'], 
					    'line2' 		=> $contact['refresh_token'], 
					    'locality' 		=> $contact['locality'], 
					    'region' 		=> $contact['region'], 
					    'postal_code' 	=> $contact['postal_code'], 
					    'zip_code' 		=> $contact['zip_code'], 
					    'zip_four' 		=> $contact['zip_four'], 
					    'country_code' 	=> $contact['country_code'], 
					    'owner_id' 		=> $contact['owner_id'], 
					    'middle_name' 	=> $contact['middle_name'], 
					    'given_name' 	=> $contact['given_name'], 
					    'email_status' 	=> $contact['email_status'], 
					    'company' 		=> $contact['company'], 
					    'family_name' 	=> $contact['family_name'], 
					    'last_updated' 	=> $contact['last_updated'], 
					    'date_created' 	=> $contact['date_created']
					),array('id' => $contact['id']));
			}else{
				$query_insert = $wpdb->insert($wpdb->prefix.'contacts_infusionSoft', array(
				    'id' 			=> $contact['id'],
				    'email' 		=> $contact['email'],
				    'line1' 		=> $contact['line1'], 
				    'line2' 		=> $contact['refresh_token'], 
				    'locality' 		=> $contact['locality'], 
				    'region' 		=> $contact['region'], 
				    'postal_code' 	=> $contact['postal_code'], 
				    'zip_code' 		=> $contact['zip_code'], 
				    'zip_four' 		=> $contact['zip_four'], 
				    'country_code' 	=> $contact['country_code'], 
				    'owner_id' 		=> $contact['owner_id'], 
				    'middle_name' 	=> $contact['middle_name'], 
				    'given_name' 	=> $contact['given_name'], 
				    'email_status' 	=> $contact['email_status'], 
				    'company' 		=> $contact['company'], 
				    'family_name' 	=> $contact['family_name'], 
				    'last_updated' 	=> $contact['last_updated'], 
				    'date_created' 	=> $contact['date_created'] 
				));
			}
		}
		wp_die();
	}

	function disconnectApp(){
		global $wpdb;
		$tableOne = $wpdb->prefix."infusion_tokenDetails";
		$wpdb->query("TRUNCATE TABLE ".$tableOne);
		$tableTwo = $wpdb->prefix."contacts_infusionSoft";
		$wpdb->query("TRUNCATE TABLE ".$tableTwo);
		$tableThree = $wpdb->prefix."tags_infusionSoft";
		$wpdb->query("TRUNCATE TABLE ".$tableThree);
		$tableFour = $wpdb->prefix."tags_of_contact_infusionSoft";
		$wpdb->query("TRUNCATE TABLE ".$tableFour);
		$tableFive = $wpdb->prefix."tagged_contacts_infusionSoft";
		$wpdb->query("TRUNCATE TABLE ".$tableFive);
		echo json_encode(['status'=>200]);
		wp_die();
	}
}
new ISAjax();