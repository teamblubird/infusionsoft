<?php 
class ISDb{
	
	function __construct(){}

	public static function createAccessTokenTable(){
		global $wpdb;
		$sql = "CREATE TABLE `". $wpdb->prefix ."infusion_tokenDetails` (
		  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		  `access_token` varchar(128) NOT NULL ,
		  `token_type` varchar(128) NOT NULL,
		  `expires_in` varchar(128) NOT NULL,
		  `refresh_token` varchar(128) NOT NULL,
		  `scope` varchar(128) NOT NULL,
		  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		);";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	dbDelta( $sql );

	}

	public static function createSettingsCredTable(){
		global $wpdb;
		$sql = "CREATE TABLE `". $wpdb->prefix ."settings_cred_for_infusionSoft` (
		  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		  `client_id` varchar(128) NOT NULL ,
		  `client_secret` varchar(128) NOT NULL,
		  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		);";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	dbDelta( $sql );

	}

	public static function createContactTable(){
		global $wpdb;
		$sql = "CREATE TABLE `". $wpdb->prefix ."contacts_infusionSoft` (
		  `id` int(10) NOT NULL PRIMARY KEY,
		  `email` varchar(128) NOT NULL ,
		  `line1` varchar(128),
		  `line2` varchar(128),
		  `locality` varchar(128),
		  `region` varchar(128),
		  `postal_code` int(10),
		  `zip_code` int(10),
		  `zip_four` varchar(128),
		  `country_code` varchar(128),
		  `owner_id` varchar(128),
		  `middle_name` varchar(128),
		  `given_name` varchar(128) ,
		  `email_status` varchar(128),
		  `company` varchar(128),
		  `family_name` varchar(128),
		  `last_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		);";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	dbDelta( $sql );
	}

	public static function createTagTable(){
		global $wpdb;
		$sql = "CREATE TABLE `". $wpdb->prefix ."tags_infusionSoft` (
		  `id` int(10) NOT NULL PRIMARY KEY,
		  `tag_name` varchar(128) NOT NULL ,
		  `description` varchar(128),
		  `cat_id` int(10),
		  `cat_name` varchar(128),
		  `cat_description` varchar(128),
		  `count` int(10) NOT NULL,
		  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		);";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	dbDelta( $sql );
	}

	public static function createTagsOfContactTable(){
		global $wpdb;
		$sql = "CREATE TABLE `". $wpdb->prefix ."tags_of_contact_infusionSoft` (
		  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		  `tag_id` int(10),
		  `contact_id` int(10),
		  `tag_name` varchar(128) NOT NULL ,
		  `category` varchar(128),
		  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		);";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	dbDelta( $sql );
	}

	public static function createTaggedContactsTable(){
		global $wpdb;
		$sql = "CREATE TABLE `". $wpdb->prefix ."tagged_contacts_infusionSoft` (
		  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		  `tag_id` int(10) NOT NULL,
		  `contact_id` int(10) NOT NULL,
		  `first_name` varchar(128) NOT NULL ,
		  `last_name` varchar(128),
		  `email` varchar(128) NOT NULL,
		  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
		);";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    	dbDelta( $sql );
	}

	public static function getAccessTokenFromDb(){
		$table = 'infusion_tokenDetails';
		$tableData = getTableData($table);
		$tableData = json_decode(json_encode($tableData), true);
		if (!empty($tableData)) {
			$tableDataArray = $tableData[0];
			return $access_token = $tableDataArray['access_token'];
		} else {
			return "empty";
		}
	}
	public static  function dataForTokenValidity(){
		
	}

	public static function deleteAccessTokenTable() {
		self::deleteTable('infusion_tokenDetails');
	}

	public static function deleteSettingsCredTable() {
		self::deleteTable('settings_cred_for_infusionSoft');
	}

	public static function deleteContactTable() {
		self::deleteTable('contacts_infusionSoft');
	}

	public static function deleteTagTable() {
		self::deleteTable('tags_infusionSoft');
	}

	public static function deleteTagsOfContactTable() {
		self::deleteTable('tags_of_contact_infusionSoft');
	}

	public static function deleteTaggedContactsTable() {
		self::deleteTable('tagged_contacts_infusionSoft');
	}
	
	public static function deleteTable($tableName) {
	    global $wpdb;
	    $tableName = $wpdb->prefix .$tableName;
	    $wpdb->query( "DROP TABLE IF EXISTS $tableName" );
	}

	public static function getSettingsCred(){
		$table = 'settings_cred_for_infusionSoft';
		return $tableData = getTableData($table);
	}

	public static function getTableData($tableName) {
	    global $wpdb;
	    $tableName = $wpdb->prefix.$tableName;
	    $sql = "SELECT * FROM ". $tableName;
	    $results = $wpdb->get_results($sql);
		return $results;
	}

	public static function firstTimeInsert($data){
		global $wpdb;
		if ($data['access_token'] != "" && $data['token_type'] != "" && $data['expires_in'] != "" && $data['refresh_token'] != "" && $data['scope'] != "")  {
			$wpdb->insert($wpdb->prefix.'infusion_tokenDetails', array(
			    'access_token' => $data['access_token'],
			    'token_type' => $data['token_type'],
			    'expires_in' => $data['expires_in'], 
			    'refresh_token' => $data['refresh_token'], 
			    'scope' => $data['scope'], 
			));
			return 1;
		} else {
			return 0;
		}
	}

	public static function SettingsCredInsert(){
		global $wpdb;
		$wpdb->insert($wpdb->prefix.'settings_cred_for_infusionSoft', array(
		    'client_id' => 'wm5fzcc2v47vk8ksrsev7838',
		    'client_secret' => '6bAyUPJnWu'
		));
		 
	}

	public static function updateTokenTable($data){
		global $wpdb;
		$wpdb->update($wpdb->prefix.'infusion_tokenDetails', array(
			'access_token' => $data['access_token'],
		    'token_type' => $data['token_type'],
		    'expires_in' => $data['expires_in'], 
		    'refresh_token' => $data['refresh_token'], 
		    'scope' => $data['scope'],
		),array('id'=>1));
	}

	public static function getContactDataFromDb(){
		$table = 'contacts_infusionSoft';
		$tableData = getTableData($table);
		return $tableData = json_decode(json_encode($tableData), true);
	}

	public static function getContactIdDb(){
		global $wpdb;
		$table = 'contacts_infusionSoft';
		$tableName = $wpdb->prefix.$table;
	    $sql = "SELECT id FROM ". $tableName;
	    $results = $wpdb->get_results($sql);
		return $results = json_decode(json_encode($results), true);
	}

	public static function getTagDataFromDb(){
		$table = 'tags_infusionSoft';
		$tableData = getTableData($table);
		return $tableData = json_decode(json_encode($tableData), true);
	}

	public static function taggedContactsByTagId($tagId){
		global $wpdb;
		$table = 'tagged_contacts_infusionSoft';
	    $tableName = $wpdb->prefix.$table;
	    $sql = "SELECT * FROM ".$tableName." WHERE tag_id =".$tagId;
	    $results = $wpdb->get_results($sql);
		return $results = json_decode(json_encode($results), true);
	}

	public static function tagsOfContactByContactId($contactId){
		global $wpdb;
		$table = 'tags_of_contact_infusionSoft';
	    $tableName = $wpdb->prefix.$table;
	    $sql = "SELECT * FROM ".$tableName." WHERE contact_id =".$contactId;
	    $results = $wpdb->get_results($sql);
		return $results = json_decode(json_encode($results), true);
	}
} new ISDb();