<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://blubirdinteractive.com/
 * @since      1.0.0
 *
 * @package    Infusion_Soft
 * @subpackage Infusion_Soft/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Infusion_Soft
 * @subpackage Infusion_Soft/admin
 * @author     Blubird Interactive Ltd <mail2technerd@gmail.com>
 */
class Infusion_Soft_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Infusion_Soft_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Infusion_Soft_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/infusion-soft-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-font-awesome', plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css', array(), $this->version, 'all' );


	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Infusion_Soft_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Infusion_Soft_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/infusion-soft-admin.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name.'-bootstrap', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, false );

	}
	public function admin_menus_items() {
		add_menu_page('infusion-soft', 'infusion-soft', 'manage_options', 'infusion-soft', function (){ $this->infusion_soft(); } ,'',10);
		if(getAccessTokenFromDb() != "empty"){
			$table = 'infusion_tokenDetails';
			$tableData = getTableData($table);
			$updated_at = $tableData[0]->updated_at;
			if ($updated_at != '0000-00-00 00:00:00') {
				$created_at = strtotime($updated_at);
			} else {
				$created_at = strtotime($tableData[0]->created_at);
			}
			$expires_in = $tableData[0]->expires_in;

			if(isValidToken($created_at,$expires_in) != 401){
				add_submenu_page('infusion-soft', 'Contacts', 'Contacts', 'manage_options', 'contacts', function (){ $this->infusion_contacts(); } );
				add_submenu_page('infusion-soft', 'Tags', 'Tags', 'manage_options', 'tags', function (){ $this->infusion_tags(); } );
				add_submenu_page('infusion-soft', 'Categories', 'Categories', 'manage_options', 'categories', function (){ $this->infusion_categories(); } );
				
			}
		}
	}

	public function infusion_soft() {
		include plugin_dir_path( dirname( __FILE__ ) ).'admin/partials/infusion-soft-admin-display.php';
	}

	public function infusion_contacts() {
		include plugin_dir_path( dirname( __FILE__ ) ).'admin/partials/infusion-soft-contacts-display.php';
	}

	public function infusion_tags() {
		include plugin_dir_path( dirname( __FILE__ ) ).'admin/partials/infusion-soft-tags-display.php';
	}

	public function infusion_categories() {
		include plugin_dir_path( dirname( __FILE__ ) ).'admin/partials/infusion-soft-categories-display.php';
	}
}
