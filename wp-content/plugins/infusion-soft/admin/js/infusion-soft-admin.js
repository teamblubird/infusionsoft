(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	// $("#submitCred").on("click",function(){
	//  	var clientID = $("#clientID").val();
	//  	var clientSecret = $("#clientSecret").val();
	//  	var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";

 //        var ajaxData = {

 //          'action'  	: 'authenticateWithCred',

 //          'clientID'  	: clientID,

 //          'clientSecret': clientSecret,

 //        };

 //            $.ajax({
	// 			url: ajaxUrl,
	// 			method: 'POST',
	// 			data: ajaxData,
	// 			success: function ( data ) {
	// 				console.log(data);
	// 			},
 //                error: function(e) {
 //                	// alert("Something Went Wrong! Please try again later");
 //               		console.log(e);

 //                }

 //            });
	//  });

})( jQuery );
