<?php 
class ISConnectedprofile {
	public static function showConnectedProfile($profile,$company,$contacts){
		// echo "<pre>";print_r($company);echo "</pre>";exit();
		$companyAddress = $company['address'];
		$access_token = getAccessTokenFromDb();
		$contacts = $contacts; 
		?>
		<div>
		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#connectedUser" role="tab" data-toggle="tab">User Details</a></li>
		    <li role="presentation"><a href="#company" role="tab" data-toggle="tab">Company</a></li>
		    <li role="presentation"><input type="submit" value="Disconnect" id="disconnect" class="btn btn-danger" data-toggle="modal" data-target="#confirmDisconnect"></li>
		  </ul>

		  <!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="connectedUser">
					<p>Name: <?php echo $profile['given_name']." ".$profile['middle_name']." ".$profile['family_name']; ?></p>
					<p>Email: <?php echo $profile['email']; ?></p>
					<p>infusionsoft Id: <?php echo $profile['infusionsoft_id']; ?></p>
				</div>
				<div role="tabpanel" class="tab-pane" id="company">
					<label for="">Company Details</label>
					<p>Name: <?php echo $company['name']; ?></p>
					<p>Email: <?php echo $company['email']; ?></p>
					<p>Website: <a href="<?php echo $company['website']; ?>" target="_blank"><?php echo $company['website']; ?></a></p>
					<p>Address
						<p><?php echo $companyAddress['line1'].', '.$companyAddress['locality'].', '.$companyAddress['region'].'-'.$companyAddress['zip_code'].', '.$companyAddress['country_code']; ?></p>
					</p>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div id="confirmDisconnect" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		    	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Are You Sure To Disconnect</h4>
		      </div>
		      <div class="modal-body disconnectMsg hidden">
					<p>Disconnected Successfully</p>
		      </div>
		      <div class="modal-footer disconnectOptions" style="text-align: center !important;">
		        <input type="submit" value="Yes" id="disconnectYes" class="btn btn-success disconnectFonfirmBtn">
			    <input type="submit" value="No" id="disconnectNo" class="btn btn-danger disconnectFonfirmBtn">
		      </div>
		    </div>

		  </div>
		</div>

		<script>
			(function($){
				'use strict';
				var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";
				var access_token = '<?php echo $access_token; ?>';

				$("#disconnectYes").on("click",function(){
			        var ajaxData = {
			          'action'  		: 'disconnectApp'
			        };
			        $.ajax({
						url: ajaxUrl,
						method: 'POST',
						data: ajaxData,
						success: function ( data ) {
							console.log(data);
							var obj = JSON.parse(data);
	                		var app_status = obj.status;
	                		if (app_status == 200) {
	                			$(".disconnectMsg").removeClass("hidden");
	                			$(".disconnectOptions").addClass("hidden");
	                			setTimeout(function(){
							        location.reload();
							    }, 2000);
	                		}
						},
		                error: function(e) {
		                	// alert("Something Went Wrong! Please try again later");
		               		console.log(e);
		                },
		                ajaxStop: function(data){
		                	alert(window.location.href)
		                }
		            });
				});

			})(jQuery);
		</script>
		<?php
	}
}
new ISConnectedprofile();