<?php 
$table = 'infusion_tokenDetails';
$tableData = getTableData($table);
$tableData = json_decode(json_encode($tableData), true);
// echo "<pre>";print_r($tableData);echo "</pre>";exit();
$tableDataArray = $tableData[0];
$access_token = $tableDataArray['access_token'];
$contacts = getContactDataFromDb();
$countries = getCountries($access_token);
?>
<div class="contactsPage">
	<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#contacts" role="tab" data-toggle="tab">All Contacts</a></li>
			<li role="presentation"><a href="#addContact" role="tab" data-toggle="tab">Add New</a></li>
			<li role="presentation"><a href="#country_list" role="tab" data-toggle="tab">All Countries with Country Code</a></li>

			<li>
				<input type="submit" value="Sync" class="btn btn-success" id="syncAllContacts">
			</li>
			 
		</ul>


	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane fade in active" id="contacts">
			<div class="showContacts table-responsive">
				<?php //echo "<pre>";print_r($contacts);echo "</pre>";exit(); ?>
				<table class="table table-bordered table-hover">
					<thead>
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Address</th>
						<th>City</th>
						<th>State</th>
						<th>Postal Code</th>
						<th>Country Code</th>
						<th>Tags</th>
						<th>Action</th>
					</thead>
					<tbody>
						<?php
							$count = 1; 
							foreach ($contacts as $key => $contact) {
						 ?>
						<tr id="singleContact_<?php echo $count; ?>">
							<td class="hidden">
								<input class="singleContactId" type="text" value="<?php echo $contact['id']; ?>">
								<?php echo $contact['id']; ?>	
							</td>
							<td class="singleContactData"><?php echo $contact['given_name']; ?></td>
							<td class="editContact hidden">
									<input type="text" id="given_name" value="<?php echo $contact['given_name']; ?>" class="form-control updateContactName">
							</td>
							<td class="singleContactData"><?php echo $contact['email']; ?></td>
							<td class="editContact hidden">
									<input type="text" id="email" value="<?php echo $contact['email']; ?>" class="form-control updateContactEmail">
							</td>
							<td class="singleContactData"><?php if(@$contact['number']){ echo @$contact['number']; }?></td>
							<td class="editContact hidden">
									<input type="text" id="number" value="<?php echo @$contact['number']; ?>" class="form-control updateContactPhone">
							</td>
							<td class="singleContactData"><?php echo $contact['line1']; ?></td>
							<td class="editContact hidden">
									<input type="text" id="line1" value="<?php echo $contact['line1']; ?>" class="form-control updateContactAddress">
							</td>
							<td class="singleContactData"><?php echo $contact['locality']; ?></td>
							<td class="editContact hidden">
									<input type="text" id="locality" value="<?php echo $contact['locality']; ?>" class="form-control updateContactLocality">
							</td>
							<td class="singleContactData"><?php echo $contact['region']; ?></td>
							<td class="editContact hidden">
									<input type="text" id="region" value="<?php echo $contact['region']; ?>" class="form-control updateContactRegion">
							</td>
							<td class="singleContactData"><?php echo $contact['postal_code']; ?></td>
							<td class="editContact hidden">
									<input type="text" id="postal_code" value="<?php echo $contact['postal_code']; ?>" class="form-control updateContactPostal_code">
							</td>
							<td class="singleContactData country_code"><?php echo $contact['country_code']; ?></td>
							<td class="editContact hidden">
									<select name="" id=" addContactCountryCode" class="update_country_code updateContactCountryCode">
									<?php foreach ($countries as $key => $value) {
									?>	
										<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
									<?php } ?>	
									</select>
							</td>
							<td><input type="submit" value="Tags" class="showTagBtn btn btn-primary" data-toggle="modal" data-target="#myModal"></td>
							<td>
								<input type="submit" value="Edit" id="" class="btn btn-warning editContactBtn singleContactData">
								<input type="submit" value="Delete" id="" class="btn btn-danger deleteContactBtn singleContactData">
								<input type="submit" value="Save" id="" class="btn btn-primary updateContactBtn editContact hidden">
							</td>
						</tr>
						<?php $count++; } ?>
					</tbody>
				</table>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade" id="addContact">
			<div class="addContact">
				<div class="form-group">
					<input type="text" placeholder="Name" id="addContactName">
				</div>
				<div class="form-group">
					<input type="text" placeholder="Email Address" id="addContactEmail">
				</div>
				<div class="form-group">
					<textarea placeholder="Address" id="addContactAddress" cols="30" rows="10"></textarea>
				</div>
				<div class="form-group">
					<input type="text" placeholder="City" id="addContactLocality">
				</div>
				<div class="form-group">
					<input type="text" placeholder="Postal Code" id="addContactPostal_code">
				</div>
				<div class="form-group">
					<select name="" id="addContactCountryCode">
						<option value="">--Select--</option>
					<?php
						foreach ($countries as $key => $country) {
					?>
						<option value="<?php echo $key; ?>"><?php echo $country; ?></option>
					<?php } ?>
					</select>
					<!-- <input type="text" placeholder="Country Code" id="addContactCountryCode"> -->
				</div>
				<div class="form-group hidden">
					<select name="" id="addContactRegion">
						<option value=""></option>
					</select>
				</div>
				<div class="form-group">
					<p id="form_validate_msg"></p>
				</div>
				<div class="form-group">
					<input type="submit" value="Submit" id="addContactBtn" class="btn btn-primary">
				</div>
			</div>
			<div class="addContactMsg hidden">
				<p class="text-success">Contact added successfully</p>
			</div>
			<div class="addContactErr hidden">
				<p class="text-danger">Contact could not be added due to any field error. Please check the all input fields and submit again</p>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade" id="country_list">
			<?php echo "<pre>";print_r($countries);echo "</pre>"; ?>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tags</h4>
      </div>
      <div class="modal-body">
      	<div class="tagsOfContact table-responsive">
      		<table class="table table-bordered table-hover">
      			<thead>
      				<th>SI</th>
      				<th class="hidden">ID</th>
      				<th>Name</th>
      				<th>category</th>
      				<th>Action</th>
      			</thead>
      			<tbody>
      			</tbody>
      		</table>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- End Modal -->
<script>
	(function( $ ) {
		'use strict';
		var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";
		var access_token = "<?php echo $access_token; ?>";
		$("#addContactBtn").on("click",function(){
			var addContactName = $("#addContactName").val();
			var addContactEmail = $("#addContactEmail").val();
		 	var addContactPhone = $("#addContactPhone").val();
		 	var addContactAddress = $("#addContactAddress").val();
		 	var addContactLocality = $("#addContactLocality").val();
		 	var addContactRegion = $("#addContactRegion").val();
		 	var addContactPostal_code = $("#addContactPostal_code").val();
		 	var addContactCountryCode = $("#addContactCountryCode").val();
		 	var access_token = "<?php echo $access_token ?>";

		 	function validate(){
		 		validate = ''
		 		if(addContactEmail.trim() == ""){
		 			validate = validate + "Email Required <br>";
		 			$('#addContactEmail').parent('.form-group').addClass('has-error');
		 		} else {
		 			$('#addContactEmail').parent('.form-group').removeClass('has-error');
		 		}

		 		if(addContactCountryCode.trim() == ""){
		 			validate = validate + "Country Required <br>";
		 			$('#addContactCountryCode').parent('.form-group').addClass('has-error');
		 		} else {
		 			$('#addContactCountryCode').parent('.form-group').removeClass('has-error');
		 		}
		 		return validate;
		 	}

		 	validate = validate();
		 	if(validate == ''){
		        var ajaxData = {
		          'action'  			: 'addContact',
		          'access_token'		: access_token,
		          'addContactName'		: addContactName,
		          'addContactEmail' 	: addContactEmail,
		          'addContactAddress'	: addContactAddress,
		          'addContactLocality'	: addContactLocality,
		          'addContactRegion'	: addContactRegion,
		          'addContactPostal_code': addContactPostal_code,
		          'addContactCountryCode': addContactCountryCode
		        };

	            $.ajax({
					url: ajaxUrl,
					method: 'POST',
					data: ajaxData,
					success: function ( data ) {
						console.log(data);
						var obj = JSON.parse(data);
	                	var app_status = obj.status;
	                	if(app_status == 201){
							$(".addContactMsg").removeClass("hidden");
							$(".addContact").addClass("hidden");
	                	} else {
	                		$(".addContactErr").removeClass("hidden");
	                	}
					},
	                error: function(e) {
	                	// alert("Something Went Wrong! Please try again later");
	               		console.log(e);
	                },
	                ajaxStop: function(data){
	                	alert(window.location.href)
	                }
	            });
		 	} else {
		 		$("#form_validate_msg").html();
		 	}
		});

		$("#addContactCountryCode").on("change",function(){
			var country_code = $(this).val();
			var ajaxData = {
		          'action'  			: 'getProvinceLists',
		          'access_token'		: access_token,
		          'country_code'		: country_code,
		    }
		    $.ajax({
				url: ajaxUrl,
				method: 'POST',
				data: ajaxData,
				success: function ( data ) {
					console.log(data);
					var obj = JSON.parse(data);
                	var app_status = obj.status;					
                	var provinces = obj.provinces;
                	if (app_status == 200) {
                		var options = "";
                		$.each( provinces, function( key, value ) {
							// alert( key + ": " + value );
							options += '<option value="'+key+'">'+value+'</option>'

						});
						$("#addContactRegion").parents('.form-group').removeClass("hidden");	
						$("#addContactRegion").html(options);
                	}				
				},
                error: function(e) {
                	// alert("Something Went Wrong! Please try again later");
               		console.log(e);
                }
            });
		});

		$(".editContactBtn").on("click",function(){
			var rowId = $(this).parents("tr").attr("id");
			$("#"+ rowId + " .singleContactData").addClass("hidden");
			$("#"+ rowId + " .editContact").removeClass("hidden");
			var country_code = $("#"+ rowId + " .country_code").text();
			$(this).parents("#" + rowId).find(".update_country_code").val(country_code);
		});

		$(".updateContactBtn").on("click",function(){
			var rowId = $(this).parents("tr").attr("id");
			var contactId = $("#" + rowId + " .singleContactId").val();
			var updateContactName = $("#" + rowId + " .updateContactName").val();
			var updateContactEmail = $("#" + rowId + " .updateContactEmail").val();
		 	var updateContactPhone = $("#" + rowId + " .updateContactPhone").val();
		 	var updateContactAddress = $("#" + rowId + " .updateContactAddress").val();
		 	var updateContactLocality = $("#" + rowId + " .updateContactLocality").val();
		 	var updateContactRegion = $("#" + rowId + " .updateContactRegion").val();
		 	var updateContactPostal_code = $("#" + rowId + " .updateContactPostal_code").val();
		 	var updateContactCountryCode = $("#" + rowId + " .updateContactCountryCode").val();

			var ajaxData = {
		          'action'  			: 'updateContactById',
		          'access_token'		: access_token,
		          'contactId'			: contactId,
		          'updateContactName'		: updateContactName,
		          'updateContactEmail' 	: updateContactEmail,
		          'updateContactPhone' 	: updateContactPhone,
		          'updateContactAddress'	: updateContactAddress,
		          'updateContactLocality'	: updateContactLocality,
		          'updateContactRegion'	: updateContactRegion,
		          'updateContactPostal_code': updateContactPostal_code,
		          'updateContactCountryCode': updateContactCountryCode
		    }
		    $.ajax({
				url: ajaxUrl,
				method: 'POST',
				data: ajaxData,
				success: function ( data ) {
					console.log(data);
					var obj = JSON.parse(data);
                	var app_status = obj.status;
                	if (app_status == 200) {
                		alert("Updated successfully");
                		location.reload();
                	}				
				},
                error: function(e) {
                	// alert("Something Went Wrong! Please try again later");
               		console.log(e);
                }
            });
		});
		$(".deleteContactBtn").on("click",function(){
			var rowId = $(this).parents("tr").attr("id");
			var contactId = $("#" + rowId + " .singleContactId").val();

			var ajaxData = {
		          'action'  			: 'deleteContactById',
		          'access_token'		: access_token,
		          'contactId'			: contactId
		    }

		    $.ajax({
				url: ajaxUrl,
				method: 'POST',
				data: ajaxData,
				success: function ( data ) {
					console.log(data);
					var obj = JSON.parse(data);
                	var app_status = obj.status;
                	if (app_status == 204) {
                		alert("Deleted successfully");
                		location.reload();
                	}				
				},
                error: function(e) {
                	// alert("Something Went Wrong! Please try again later");
               		console.log(e);
                }
            });
		});

		$(".showTagBtn").on("click",function(){
			var rowId = $(this).parents("tr").attr("id");
			var contactId = $("#" + rowId + " .singleContactId").val();

			var ajaxData = {
				'action'  			: 'showTagsByContactId',
				'contactId'			: contactId
		    }

		    $.ajax({
				url: ajaxUrl,
				method: 'POST',
				data: ajaxData,
				success: function ( data ) {
					console.log(data);
					$(".tagsOfContact tbody").html(data);			
				},
                error: function(e) {
                	// alert("Something Went Wrong! Please try again later");
               		console.log(e);
                }
            });
		});

		$("#syncAllContacts").on("click",function(){
			var ajaxData = {
				'action'  			: 'syncAllContacts',
				'access_token'		:  access_token
		    }

		    $.ajax({
				url: ajaxUrl,
				method: 'POST',
				data: ajaxData,
				success: function ( data ) {
					console.log(data);			
				},
                error: function(e) {
                	// alert("Something Went Wrong! Please try again later");
               		console.log(e);
                }
            }); 
		});
	})( jQuery );

</script>