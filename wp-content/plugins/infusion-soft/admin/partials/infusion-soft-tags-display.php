<?php 
	$access_token = getAccessTokenFromDb();
	// $tags = getTags($access_token);
	// echo "<pre>";print_r($access_token);echo "</pre>";exit();
	$tags = getTagDataFromDb();
	// echo "<pre>";print_r($tags);echo "</pre>";exit();
?>
<div class="tagPage">

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#tags" role="tab" data-toggle="tab">Tags</a></li>
		<li role="presentation"><a href="#addNewTag" role="tab" data-toggle="tab">Add New Tag</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="tags">
			<div class="showAllTags table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<th>SI</th>
						<th>Name</th>
						<th>Description</th>
						<th>Category</th>
						<th>Count</th>
					</thead>
					<tbody>
					<?php
					$count = 1;
						foreach ($tags as $key => $tag) {
					?>	
						<tr id="tag_<?php echo $count;?>">
							<td><?php echo $count; ?></td>
							<td class="hidden tagId"><?php echo $tag['id']; ?></td>
							<td><?php echo $tag['tag_name']; ?></td>
							<td><?php echo $tag['description']; ?></td>
							<td><?php echo $tag['cat_name']; ?></td>
							<td>
								<input type="submit" value="<?php echo $tag['count']; ?>" id="" class="btn btn-primary showContacts" data-toggle="modal" data-target="#myModal">
							</td>
						</tr>
					<?php $count++;} ?>	
					</tbody>
				</table>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane" id="addNewTag">
			<div class="addTag">
				<div class="form-group">
					<input type="text" placeholder="Tag Name" id="addTagName">
				</div>
				<div class="form-group">
					<textarea placeholder="Tag Description" id="addTagDescription" cols="30" rows="10"></textarea>
				</div>
				<!-- <div class="form-group">
					<input type="text" placeholder="Category Name" id="addTagCat">
				</div> -->
				<!-- <div class="form-group">
					<textarea placeholder="Category Description" id="addTagAddress" cols="30" rows="10"></textarea>
				</div> -->
				<div class="form-group">
					<input type="submit" value="Submit" id="addTagBtn" class="btn btn-primary">
				</div>
			</div>
			<div class="addTagMsg hidden">
				<p class="text-success">Tag added successfully</p>
			</div>
			<div class="addTagErr hidden">
				<p class="text-danger">Tag could not be added due to any field error. Please check the all input fields and submit again</p>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tagged Contacts</h4>
      </div>
      <div class="modal-body">
      	<div class="taggedContactsLists table-responsive">
      		<table class="table table-bordered table-hover">
      			<thead>
      				<th>SI</th>
      				<th>Name</th>
      				<th>Email</th>
      			</thead>
      			<tbody>
      			</tbody>
      		</table>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- End Modal -->

<script>
	(function($){
		'use strict';
		var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";
		var access_token = "<?php echo $access_token; ?>";
		$("#addTagBtn").on("click",function(){
			var tagName 		= $("#addTagName").val(); 
			var tagDescription 	= $("#addTagDescription").val();
			function validate(){
		 		validate = ''
		 		if(tagName.trim() == ""){
		 			validate = validate + "Tag Name Required <br>";
		 			$('#addTagName').parent('.form-group').addClass('has-error');
		 		} else {
		 			$('#addTagName').parent('.form-group').removeClass('has-error');
		 		}

		 		if(tagDescription.trim() == ""){
		 			validate = validate + "Tag Description Required <br>";
		 			$('#addTagDescription').parent('.form-group').addClass('has-error');
		 		} else {
		 			$('#addTagDescription').parent('.form-group').removeClass('has-error');
		 		}
		 		return validate;
			}
			validate = validate();
		 	if(validate == ''){
		        var ajaxData = {
		          'action'  			: 'addTag',
		          'access_token'		: access_token,
		          'tagName'				: tagName,
		          'tagDescription'	 	: tagDescription
		        };

	            $.ajax({
					url: ajaxUrl,
					method: 'POST',
					data: ajaxData,
					success: function ( data ) {
						console.log(data);
						var obj = JSON.parse(data);
                		var app_status = obj.status;
                		if (app_status == 200) {
                			$(".addTagMsg").removeClass("hidden");
                			$(".addTagErr").addClass("hidden");
                		} else {
                			$(".addTagMsg").addClass("hidden");
                			$(".addTagErr").removeClass("hidden");
                		}
						
					},
	                error: function(e) {
	                	// alert("Something Went Wrong! Please try again later");
	               		console.log(e);
	                }
	            });
		 	} else {
		 		$("#form_validate_msg").html();
		 	}
		});

		$(".showContacts").on("click",function(){
			var rowId = $(this).parents("tr").attr("id");
			var tagId = $("#" + rowId + " .tagId").text().trim();
			var ajaxData = {
		          'action'  			: 'taggedContactsById',
		          'tagId'				: tagId
	        };

	        $.ajax({
				url: ajaxUrl,
				method: 'POST',
				data: ajaxData,
				success: function ( data ) {
					console.log(data);
					// var obj = JSON.parse(data);
            		// var app_status = obj.status;
            		$(".taggedContactsLists tbody").html(data);


				},
                error: function(e) {
                	// alert("Something Went Wrong! Please try again later");
               		console.log(e);
                }
            });
		});


	})(jQuery);
</script>