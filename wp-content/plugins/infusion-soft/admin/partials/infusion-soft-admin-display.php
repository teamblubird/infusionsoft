<?php
$infusionClientId = getClientId();
$infusionClientSecret = getClientSecret();
// echo getAccessTokenFromDb();
if(getAccessTokenFromDb() != "empty"){
	$table = 'infusion_tokenDetails';
	$tableData = getTableData($table);
	// echo "<pre>";print_r($tableData);echo "</pre>";exit();
	$updated_at = $tableData[0]->updated_at;
	// echo $updated_at;exit();
	if ($updated_at != '0000-00-00 00:00:00') {
		$created_at = strtotime($updated_at);
	} else {
		$created_at = strtotime($tableData[0]->created_at);
	}
	$expires_in = $tableData[0]->expires_in;
	// echo isValidToken($created_at,$expires_in);
	$refresh_token = $tableData[0]->refresh_token;
	if(isValidToken($created_at,$expires_in) == 401){
		$newToken = getRefreshToken($infusionClientId,$infusionClientSecret,$refresh_token);
		updateTokenTable($newToken);
		$showCredForm = 0;
	} else {
		$showCredForm = 0;
	}
} else {
	$showCredForm = 1;
}
if ($showCredForm == 1) {
?>
<div class="row">
	<div class="col-md-6">
		<p></p>
	<?php 
		if (@$_GET['code'] == "") {
	?>
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#ourApp" role="tab" data-toggle="tab">Authorize With Our App</a></li>
		<li role="presentation"><a href="#ownerApp" role="tab" data-toggle="tab">Authorize With Your App</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="ourApp">
			<div class="credentialsForm">
				<div class="form-group hidden">
					<input type="text" placeholder="Client ID" id="client_id" value="<?php echo $infusionClientId; ?>">
				</div>
				<div class="form-group hidden">
					<input type="text" placeholder="Client Secret" id="client_secret" value="<?php echo $infusionClientSecret; ?>">
				</div>
				<div class="form-group">
					<p id="form_validate_msg"></p>
				</div>
				<div class="form-group">
					<input type="submit" value="Click Here to Authorize" id="save" class="btn btn-primary">
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane" id="ownerApp">
			<div class="ownerAppDetails">
				<div class="form-group">
					<input type="text" placeholder="Access Token" id="accessToken">
				</div>
				<div class="form-group">
					<input type="text" placeholder="Refresh Token" id="refreshToken">
				</div>
				<div class="form-group">
					<p id="form_validate_msg"></p>
				</div>
				<div class="form-group">
					<input type="submit" value="Connect" id="connectApp" class="btn btn-primary">
				</div>
			</div>
			<div class="ownerAppMsg hidden">
				<p class="text-success">Connected Successfully</p>
			</div>
		</div>
	</div>
	<?php } else { ?>
		<div class="syncloader visible">
			<div class="loaderIconCenter">
				<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
			    <span class="text-muted">Please wait...</span>
			</div>
		</div>
		<script>
			(function($){
				var client_id = '<?php echo $infusionClientId; ?>';
				var client_secret = '<?php echo $infusionClientSecret; ?>';
			 	var code = '<?php echo $_GET['code']; ?>';
			 	var redirect_uri = "<?php echo site_url(); ?>/wp-admin/admin.php?page=infusion-soft";
			 	
			 	var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";
		        var ajaxData = {
		          'action'  		: 'requestAccessToken',
		          'client_id'  		: client_id,
		          'client_secret'	: client_secret,
		          'redirect_uri'	: redirect_uri,
		          'code'			: code
		        };

	            $.ajax({
					url: ajaxUrl,
					method: 'POST',
					data: ajaxData,
					success: function ( data ) {
						console.log(data);
						var obj = JSON.parse(data);
	                	var app_status = obj.status;
	                	var access_token = obj.access_token;
	                	if (app_status == 200) {
							var ajaxData = {
					          'action'  		: 'syncAll',
					          'access_token'  	: access_token
					        };

					        $.ajax({
								url: ajaxUrl,
								method: 'POST',
								data: ajaxData,
								beforesend: function(){
									$(".syncloader").addClass("visible");
									$(".syncloader").removeClass("notVisible");
								},
								success: function ( data ) {
									console.log(data);
									var obj = JSON.parse(data);
	                				var app_status = obj.status;
	                				if (app_status == 200) {
										$(".syncloader").removeClass("visible");
										$(".syncloader").addClass("notVisible");
										$('.getToken').addClass('hidden');
		                				$('.tokenSuccess').removeClass('hidden');
		                				window.setTimeout(function () {
									        location.href = "<?php echo site_url().'/wp-admin/admin.php?page=infusion-soft'; ?>";
									    }, 3000);
	                				} else {
	                					$('.getToken').addClass('hidden');
	                					$('.tokenDanger').removeClass('hidden');
	                				}
								},
				                error: function(e) {
				               		console.log(e);
				                }
				            });
	                	} else{
	                		$('.getToken').addClass('hidden');
	                		$('.tokenDanger').removeClass('hidden');
	                		// window.location.href = redirect_uri;
	                	}
					},
	                error: function(e) {
	                	// alert("Something Went Wrong! Please try again later");
	               		console.log(e);
	                }
	            });
			})(jQuery);
		</script>
		<!-- <div class="getToken">
			<div class="form-group hidden">
				<input type="text" id="client_id_response" value="<?php echo $infusionClientId; ?>">
			</div>
			<div class="form-group hidden">
				<input type="text" id="client_secret_response" value="<?php echo $infusionClientSecret; ?>">
			</div>
			<div class="form-group hidden">
				<input type="text" id="code_response" value="<?php echo $_GET['code']; ?>">
			</div>
			<div class="form-group">
				<input type="submit" value="Request For A Token" id="requestToken" class="btn btn-primary">
			</div>
		</div> -->
		<div class="tokenSuccess hidden">
			<p class="text-success">All Data Synced Successfully</p>
		</div>
		<div class="tokenDanger hidden">
			<p class="text-danger">Something Went Wrong! Please Try Again</p>
		</div>
	<?php } ?>
	</div>
</div>
<script>
	(function( $ ) {
		'use strict';
		$("#save").on("click",function(){
		 	var client_id = $("#client_id").val();
		 	var client_secret = $("#client_secret").val();
		 	var redirect_uri = window.location.href;

		 	
		 	var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";
	        var ajaxData = {
	          'action'  		: 'authenticateWithCred',
	          'client_id'  		: client_id,
	          'client_secret'	: client_secret,
	          'redirect_uri'	: redirect_uri
	        };

            $.ajax({
				url: ajaxUrl,
				method: 'POST',
				data: ajaxData,
				success: function ( data ) {
					console.log(data);
					var obj = JSON.parse(data);
                	var app_status = obj.status;
                	var client_id = obj.client_id;
                	var client_secret = obj.client_secret;
                	var authUrl = obj.authUrl;
                	if(app_status == 200){
						window.location.href = authUrl;

                	}
				},
                error: function(e) {
                	// alert("Something Went Wrong! Please try again later");
               		console.log(e);
                },
                ajaxStop: function(data){
                	alert(window.location.href)
                }
            });
		});

		$("#connectApp").on("click",function(){
			var access_token 	= $("#accessToken").val();
			var refresh_token 	= $("#refreshToken").val();
			var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";
	        var ajaxDatatoSave = {
	          'action'  		: 'saveConnectAppCred',
	          // 'action'  		: 'checkUserProvidedCred',
	          'access_token'  	: access_token,
	          'refresh_token'	: refresh_token,
	          'expires_in'		: 86400,
	          'token_type'		: 'bearer',
	          'scope'			: 'full'
	        };

	        var ajaxDataToCheck = {
	          'action'  		: 'checkUserProvidedCred',
	          'access_token'  	: access_token
	        };

	        function validation() {
	        	var validate = '';
	        	if(access_token.trim()==''){
	              validate = validate+'Access Token required<br>';
	              $('#accessToken').css('border','1px solid #f30000');
	              $('#accessToken').parent('.form-group').addClass('has-error');
	          	} else{
	              $('#accessToken').css('border','1px solid #ccc');
	              $('#accessToken').parent('.form-group').removeClass('has-error');
	          	}

	          	if(refresh_token.trim()==''){
	              validate = validate+'Refresh Token required<br>';
	              $('#refreshToken').css('border','1px solid #f30000');
	              $('#refreshToken').parent('.form-group').addClass('has-error');
	          	} else{
	              $('#refreshToken').css('border','1px solid #ccc');
	              $('#refreshToken').parent('.form-group').removeClass('has-error');
	          	}

	          	return validate;
	        }

	        var validate = validation();
	        if (validate == "") {
	        	$.ajax({
					url: ajaxUrl,
					method: 'POST',
					data: ajaxDataToCheck,
					beforeSend: function(){
						$('.loader').show();
					},
					success: function ( data ) {
						$('.loader').hide();
						console.log(data);
						if (data == 200) {
							$.ajax({
								url: ajaxUrl,
								method: 'POST',
								data: ajaxDatatoSave,
								success: function ( data ) {
									console.log(data);
									var obj = JSON.parse(data);
			                		var app_status = obj.status;
			                		if (app_status == 200) {
			                			$(".ownerAppMsg").removeClass("hidden");
			                			setTimeout(function(){
									        location.reload();
									    }, 3000); 
			                		}
								},
				                error: function(e) {
				                	
				               		console.log(e);
				                }
				            });
						} else {
							alert("Wrong tokens submitted!! Please provide correct tokens");
						}
					},
	                error: function(e) {
	                	// alert("Something Went Wrong! Please try again later");
	               		console.log(e);
	                }
	            });
	        }
		});

	})( jQuery );

</script>
<?php } else {
	$table = 'infusion_tokenDetails';
	$tableData = getTableData($table);
	$tableData = json_decode(json_encode($tableData), true);
	$tokenData = $tableData[0];
	$access_token = $tokenData['access_token'];
	// echo $access_token;exit();
	$connectedProfile = getProfile($access_token);
	// echo "<pre>";print_r($connectedProfile);echo "</pre>";exit();
	$company = getCompany($access_token);
	// echo "<pre>";print_r(getCompany($access_token));echo "</pre>";exit();
	$contacts = getContacts($access_token);
	showConnectedProfile($connectedProfile,$company,$contacts);
}