<?php 
	$access_token = getAccessTokenFromDb();
?>

<div class="categoryPage">

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#addNewCategory" role="tab" data-toggle="tab">Add New Category</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="addNewCategory">
			<div class="addCat">
				<div class="form-group">
					<input type="text" placeholder="Category Name" id="addCatName">
				</div>
				<div class="form-group">
					<textarea placeholder="Category Description" id="addCatDescription" cols="30" rows="10"></textarea>
				</div>
				<div class="form-group">
					<input type="submit" value="Submit" id="addCatBtn" class="btn btn-primary">
				</div>
			</div>
			<div class="addCatMsg hidden">
				<p class="text-success">Category added successfully</p>
			</div>
			<div class="addCatErr hidden">
				<p class="text-danger">Category could not be added due to any field error. Please check the all input fields and submit again</p>
			</div>
		</div>
	</div>
</div>
<script>
	(function($){
		'use strict';
		var ajaxUrl =   "<?php echo admin_url('admin-ajax.php'); ?>";
		var access_token = "<?php echo $access_token; ?>";
		$("#addCatBtn").on("click",function(e){
			// e.preventDefault();
			var catName = $("#addCatName").val();
			var catDescription = $("#addCatDescription").val();
			function validate(){
				validate = ''
				if(catName.trim() == ""){
					validate = validate + "Category Name Required <br>";
					$('#addCatName').parent('.form-group').addClass('has-error');
				} else {
					$('#addCatName').parent('.form-group').removeClass('has-error');
				}

				if(catDescription.trim() == ""){
					validate = validate + "Category Description Required <br>";
					$('#addCatDescription').parent('.form-group').addClass('has-error');
				} else {
					$('#addCatDescription').parent('.form-group').removeClass('has-error');
				}
				return validate;
			}

			validate = validate();
		 	if(validate == ''){
		        var ajaxData = {
		          'action'  			: 'addCat',
		          'access_token'		: access_token,
		          'catName'				: catName,
		          'catDescription'	 	: catDescription
		        };

	            $.ajax({
					url: ajaxUrl,
					method: 'POST',
					data: ajaxData,
					success: function ( data ) {
						console.log(data);
						var obj = JSON.parse(data);
                		var app_status = obj.status;
                		if (app_status == 200) {
                			$(".addCatMsg").removeClass("hidden");
                			$(".addCatErr").addClass("hidden");
                		} else {
                			$(".addCatMsg").addClass("hidden");
                			$(".addCatErr").removeClass("hidden");
                		}
					},
	                error: function(e) {
	                	// alert("Something Went Wrong! Please try again later");
	               		console.log(e);
	                }
	            });
		 	} else {
		 		$("#form_validate_msg").html();
		 	}
		});
	})(jQuery);
</script>